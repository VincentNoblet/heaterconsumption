import d6tflow
from preprocessing.big_query_task_historical import *
from preprocessing.pipe_task import *
from preprocessing import dataflow_task
from preprocessing import weather_task, categoric_task, mode_task, consumption_task
from threading import Thread
d6tflow.settings.log_level = 'INFO'

partition_winter = {
    "2019": ("2018-10-01", "2019-03-31"),
    "2018": ("2017-10-01", "2018-03-31"),
    "2017": ("2016-10-01", "2017-03-31")
}
partition_for_all = {
    "2019": ("2018-08-20", "2019-08-19"),
    "2018": ("2017-08-20", "2018-08-19"),
    "2017": ("2016-08-20", "2017-08-19")
}
partition_winter = frozenset(partition_winter.items())
partition_for_all = frozenset(partition_for_all.items())
_partition = dict(partition_winter)
year, month, day = _partition["2019"][0].split("-")
begin_date = datetime.date(int(year), int(month), int(day))
year, month, day = _partition["2019"][1].split("-")
end_date = datetime.date(int(year), int(month), int(day))

working_rate_task_pipe = [
    (TaskLeadOverTimestamp, {"last_timestamp_period_value": 10}),
    (TaskSetWorkingRateStandBy, {}),
    (TaskRoguePeriod, {"period_max": 250}),
    (TaskRogueValueInterval, {"value_min": 0, "value_max": 100}),
    (TaskWeightLabelGatewayYear, {"partition": partition_winter}),
    (TaskWeightAverageGatewayYear, {}),
]

ambient_temperature_task_pipe = [
    (TaskLeadOverTimestamp, {"last_timestamp_period_value": 10}),
    (TaskRoguePeriod, {"period_max": 250}),
    (TaskRogueValueInterval, {"value_min": 0, "value_max": 40}),
    (TaskWeightLabelGatewayYear, {"partition": partition_winter}),
    (TaskWeightAverageGatewayYear, {}),
]

def weather_builder_pipe():
    weather_task_pipe = [
        (TaskLeadOverGatewayTimestamp, {"last_timestamp_period_value": 10}),
        (TaskRoguePeriod, {"period_max": 250}),
        (TaskRemoveOpenWeatherMapDoublon, {}),
        (weather_task.TaskLabelYear, {"partition": partition_winter}),
        (TaskAverageGatewayYear, {})
    ]
    return weather_task_pipe

TaskPipe("working_rate", "working_rate", working_rate_task_pipe),
TaskPipe("main_temp", "main_temp", weather_builder_pipe()),
