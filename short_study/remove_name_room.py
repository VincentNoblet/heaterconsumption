import nltk
from nltk import word_tokenize, pos_tag, ne_chunk
from manager.query_customize import QueryBuilder
import d6tflow
import nltk
from nltk.tag.stanford import StanfordNERTagger
import os
java_path = "C:/Program Files (x86)/Common Files/Oracle/Java/javapath/java.EXE"
#os.environ['JAVAHOME'] = java_path

st = StanfordNERTagger('stanford-french-corenlp-2018-10-05-models.jar', 'stanford-ner-2018-10-16/stanford-ner.jar')

nltk.download('averaged_perceptron_tagger')
nltk.download('punkt')
nltk.download('maxent_ne_chunker')
nltk.download('words')


class TaskGetInfoRoom(d6tflow.tasks.TaskPqPandas):
    def run(self):
        query_builder = QueryBuilder()
        query_builder.add_select("name")
        query_builder.add_from_name("`overkiz-1365.2_Data_LAB_Heater_Consumption_Prediction.2DLHCP1_info_room`")
        data = query_builder.get_execute().to_dataframe()
        self.save(data)


class TaskTokenize(d6tflow.tasks.TaskPqPandas):
    def requires(self):
        return TaskGetInfoRoom()

    def run(self):
        data = self.input().load()
        print(data)
        for data_cell in data:
            for sent in nltk.sent_tokenize(data_cell):
                tokens = nltk.tokenize.word_tokenize(sent)
                tags = st.tag(tokens)
                for tag in tags:
                    if tag[1] == 'PERSON':
                        print(tag)

TaskTokenize().run()
