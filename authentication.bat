@ECHO OFF
:BEGIN
ECHO SET GOOGLE_APPLICATION_CREDENTIALS variable
ECHO You should set the correct path of the service account's json key in the file GOOGLE_CLOUD_CREDENTIALS
ECHO Or you should autenticate thanks to the Googgle Cloud SDK application

SET /p CHOICE1="Are you running the project inside a Compute Engine VM ?(Y/[N])"
IF /I "%CHOICE1%" equ "Y" (GOTO :SKIP) ELSE (GOTO :CONTINUE)


:CONTINUE
SET /p CHOICE2="SET GOOGLE_APPLICATION_CREDENTIALS variable ?(Y/[N])"
IF /I "%CHOICE2%" equ "Y" (
    SET /p GOOGLE_APPLICATION_CREDENTIALS=<GOOGLE_APPLICATION_CREDENTIALS_PATH
    ECHO.
    ECHO Google credentials set on path %GOOGLE_APPLICATION_CREDENTIALS%

) ELSE (
    SET /p CHOICE3="Authenticate with gcloud auth ?(Y/[N])"
    IF /I "%CHOICE3%" equ "Y" (
        gcloud config set project atlantic-rad
        gcloud auth application-default login
    ) ELSE (
        ECHO You choose not to authenticate
    )
)
:SKIP
