import d6tflow
from manager.query_customize import QueryBuilder
import pandas as pd
import luigi
import numpy as np
from multiprocessing import cpu_count, Pool


class TimeTaskMixin(object):
    @luigi.Task.event_handler(luigi.Event.PROCESSING_TIME)
    def print_execution_time(self, processing_time):
        print(type(self).__name__ + ' ### PROCESSING TIME ###  ' + str(round(processing_time, 4)) + " secondes")

cores = cpu_count()  # Number of CPU cores on your system
partitions = cores  # Define as many partitions as you want


def parallelize(data, func):
    data_split = np.array_split(data, partitions)
    pool = Pool(cores)
    data = pd.concat(pool.map(func, data_split))
    pool.close()
    pool.join()
    return data


class TaskGetFirstLastConsumption(d6tflow.tasks.TaskCachePandas):

    def save_first_last_consumption_one(self, name):
        table_name = "`overkiz-1365.2_Data_LAB_Heater_Consumption_Prediction_Dev.2DLHCP1_" + name + "`"
        queryBuild = QueryBuilder()
        queryBuild.add_select_all()
        queryBuild.add_from_name(table_name)
        queryBuild.get_execute().to_dataframe().to_parquet('parquet_file/' + name + '.pq')

    def run(self):
        for name in ["first_consumption", "last_consumption", "first_cumul_consumption", "last_cumul_consumption"]:
            self.save_first_last_consumption_one(name)


class TaskMergeConsumptionIndex(d6tflow.tasks.TaskCachePandas):

    def requires(self):
        return [TaskGetFirstLastConsumption()]

    def run(self):
        self.merge_consumption("first")
        self.merge_consumption("last")

    def merge_consumption(self, first_last):
        consumption = pd.read_parquet('parquet_file/' + first_last + '_consumption.pq')
        cumul_consumption = pd.read_parquet('parquet_file/' + first_last + '_cumul_consumption.pq')
        merge = pd.merge(consumption, cumul_consumption, on="device_url", how="outer")
        merge.index = ['device_url', first_last + '_index', ]
        merge.to_parquet('parquet_file/' + first_last + "_merge_consumption" + '.pq')


class TaskMinMaxConsumptionIndex(d6tflow.tasks.TaskCachePandas, TimeTaskMixin):

    def requires(self):
        return [TaskMergeConsumptionIndex()]

    def run(self):
        first = pd.read_parquet('parquet_file/first_merge_consumption.pq')
        last = pd.read_parquet('parquet_file/last_merge_consumption.pq')
        print(first.head())
        print(last.head())
        first = first.apply(lambda x: self.apply_minmax_timestamp(x, min), axis=1)
        last = last.apply(lambda x: self.apply_minmax_timestamp(x, max), axis=1)
        print(first.head())
        print(last.head())
        first.columns = ["device_url", "first_timestamp", "first_consumption"]
        last.columns = ["device_url", "last_timestamp", "last_consumption"]
        print(first.head())
        print(last.head())
        merge = pd.merge(first, last, on="device_url")

        return merge

    def apply_minmax_timestamp(self, serie, rule):
        if pd.isna(serie[1]):
            new_serie = serie[[0, 3, 4]]
        elif pd.isna(serie[3]):
            new_serie = serie[[0, 1, 2]]
        else:
            if serie[1] == rule(serie[1], serie[3]):
                new_serie = serie[[0, 1, 2]]
            else:
                new_serie = serie[[0, 3, 4]]
        print(new_serie)
        new_serie.columns = ["device_url", "timestamp", "value"]
        return new_serie

    def get_span_life(self):
        data = pd.read_parquet('parquet_file/span_life.pq')

        data["gateway"] = data['device_url'].apply(lambda x: x.split("io://")[1].split("/")[0])
        gateway_suspect = dict()
        i = 0
        for gateway in pd.unique(data.gateway):
            df_device = data[data.gateway == gateway][["device_url", "begin_timestamp", "end_timestamp"]]

            disappear_suspect = dict()
            appear_suspect = dict()
            time_diff = []
            for device1 in df_device.itertuples():
                for device2 in df_device.itertuples():
                    if device1.device_url != device2.device_url:
                        if device1.end_timestamp <= device2.begin_timestamp:
                            if (device2.begin_timestamp - device1.end_timestamp).days <= 30:
                                disappear_suspect[device1.device_url] = (str(device1.end_timestamp.date()))
                                appear_suspect[device2.device_url] = str(device2.begin_timestamp.date())

            if appear_suspect:
                print(appear_suspect, disappear_suspect)
            if i % 100 == 0:
                print(i)
            i +=1

        return gateway_suspect

merge = TaskMinMaxConsumptionIndex().run()