from __future__ import division

import warnings
import numbers

import numpy as np
import numpy.ma as ma
from scipy import sparse
from scipy import stats

from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.utils.sparsefuncs import _get_median
from sklearn.utils.validation import check_is_fitted
from sklearn.utils.validation import FLOAT_DTYPES
from sklearn.utils.fixes import _object_dtype_isnan
from sklearn.utils import is_scalar_nan
from sklearn.utils import check_array
import pandas as pd
from sklearn.impute import SimpleImputer


class MeanMajImputer(BaseEstimator, TransformerMixin):
    def __init__(self, *,  cat_cols, num_cols):
        self.cat_cols = cat_cols
        self.num_cols = num_cols
        self.categorical_imp = SimpleImputer(strategy="most_frequent")
        self.numerical_imp = SimpleImputer(strategy="mean")

    def fit(self, X):
        categorical_mat = X[:, self.cat_cols]
        numerical_mat = X[:, self.num_cols]
        self.categorical_imp.fit(categorical_mat)
        self.numerical_imp.fit(numerical_mat)
        categorical_statistics_ = pd.DataFrame([self.categorical_imp.statistics_], columns=self.cat_cols)
        numerical_statistics_ = pd.DataFrame([self.numerical_imp.statistics_], columns=self.num_cols)
        statistics_ = pd.concat([categorical_statistics_, numerical_statistics_], axis=1)
        statistics_ = statistics_.reindex(sorted(statistics_.columns), axis=1)
        self.statistics_ = statistics_.values
        return self

    def transform(self, X):
        categorical_mat = X[:, self.cat_cols]
        numerical_mat = X[:, self.num_cols]
        categorical_frame = pd.DataFrame(self.categorical_imp.transform(categorical_mat), columns=self.cat_cols)
        numerical_frame = pd.DataFrame(self.numerical_imp.transform(numerical_mat), columns=self.num_cols)
        frame = pd.concat([categorical_frame, numerical_frame], axis=1)
        frame = frame.reindex(sorted(frame.columns), axis=1)
        return frame.values
