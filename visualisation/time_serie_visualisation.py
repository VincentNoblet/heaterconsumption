from preprocessing.pipe_task import *
from visualisation import time_serie_task

ambient_temperature_task_pipe = [
    (TaskLeadOverTimestamp, {"last_timestamp_period_value": 10}),
    (time_serie_task.TaskRoguePeriod, {"period_max": 250}),
    (time_serie_task.TaskRogueValueInterval, {"value_min": 0, "value_max": 40}),
    (time_serie_task.TaskAvgDeviceTimestampRoundNormalize, {"timestamp_inverval": 30}),
    (time_serie_task.TaskSelectDeviceTimestampValueGateway, {}),
    (time_serie_task.TaskFilterRandomGateway, {})
]
comfort_temperature_task_pipe = [
    (TaskLeadOverTimestamp, {"last_timestamp_period_value": 10}),
    (time_serie_task.TaskRoguePeriod, {"period_max": 1500}),
    (time_serie_task.TaskRogueValueInterval, {"value_min": 12, "value_max": 28}),
    (time_serie_task.TaskAvgDeviceTimestampRoundNormalize, {"timestamp_inverval": 30}),
    (time_serie_task.TaskSelectDeviceTimestampValueGateway, {}),
    (time_serie_task.TaskFilterRandomGateway, {})
]
consumption_task_pipe = [
    (TaskDiffValue, {}),
    (TaskLeadOverTimestamp, {"last_timestamp_period_value": 10}),
    (time_serie_task.TaskRogueValueInterval, {"value_min": 0, "value_max": 10 ** 8}),
    (time_serie_task.TaskAvgDeviceTimestampRoundNormalize, {"timestamp_inverval": 30}),
    (time_serie_task.TaskSelectDeviceTimestampValueGateway, {}),
    (time_serie_task.TaskFilterRandomGateway, {})
]
consumption_task_pipe_negative = [
    (TaskDiffValue, {}),
    (TaskLeadOverTimestamp, {"last_timestamp_period_value": 10}),
    (time_serie_task.TaskAvgDeviceTimestampRoundNormalize, {"timestamp_inverval": 30}),
    (time_serie_task.TaskSelectDeviceTimestampValueGateway, {}),
    (time_serie_task.TaskFilterRandomGateway, {})
]
occupancy_detection_task_pipe = [
    (TaskLeadOverTimestamp, {"last_timestamp_period_value": 10}),
    (time_serie_task.TaskRoguePeriod, {"period_max": 250}),
    (TaskFilterValue, {"value": "personInside"}),
    (TaskGenerateTimestampArray, {"timestamp_inverval": 30}),
    (time_serie_task.TaskSumDeviceTimestampRoundNormalize, {"timestamp_inverval": 30}),
    (time_serie_task.TaskSelectDeviceTimestampValueGateway, {}),
    (time_serie_task.TaskFilterRandomGateway, {})
]

setpoint_temperature_task_pipe = [
    (TaskLeadOverTimestamp, {"last_timestamp_period_value": 10}),
    (time_serie_task.TaskRoguePeriod, {"period_max": 1500}),
    (time_serie_task.TaskRogueValueInterval, {"value_min": 7, "value_max": 28}),
    (time_serie_task.TaskAvgDeviceTimestampRoundNormalize, {"timestamp_inverval": 30}),
    (time_serie_task.TaskSelectDeviceTimestampValueGateway, {}),
    (time_serie_task.TaskFilterRandomGateway, {})
]
working_rate_task_pipe = [
    (TaskLeadOverTimestamp, {"last_timestamp_period_value": 10}),
    (TaskSetWorkingRateStandBy, {}),
    (time_serie_task.TaskRoguePeriod, {"period_max": 250}),
    (time_serie_task.TaskRogueValueInterval, {"value_min": 0, "value_max": 100}),
    (time_serie_task.TaskAvgDeviceTimestampRoundNormalize, {"timestamp_inverval": 30}),
    (time_serie_task.TaskSelectDeviceTimestampValueGateway, {}),
    (time_serie_task.TaskFilterRandomGateway, {})
]

def weather_builder_pipe():
    weather_task_pipe = [
        (TaskLeadOverGatewayTimestamp, {"last_timestamp_period_value": 10}),
        (time_serie_task.TaskSelectGatewayTimestampValue, {}),
        (time_serie_task.TaskFilterRandomGateway, {})
    ]
    return weather_task_pipe


class TaskPipeVisualisation(TaskPipe):
    def set_first_task_input_output(self):
        if self.input_name == "consumption":
            input1, input2 = EasyTableName.get_consumption("consumption_index"), EasyQuery.get_consumption_table_name("cumul_consumption_index")
            self.load_task_parameter_list[0].set_input([input1, input2])
        else:
            input = EasyTableName.get_consumption(self.input_name)
            self.load_task_parameter_list[0].set_input(input)
        self.load_task_parameter_list[0].set_output(EasyTableName.get_process_visualisation(self.name))

    def set_last_task_output(self):
        output = EasyTableName.get_pretty_output_visualisation(self.output_name)
        self.load_task_parameter_list[-1].set_output(output)

intern_pipe_list = [
    TaskPipeVisualisation("ambient_temperature", "ambient_temperature", ambient_temperature_task_pipe),
    TaskPipeVisualisation("comfort_temperature", "comfort_temperature", comfort_temperature_task_pipe),
    TaskPipeVisualisation("consumption", "consumption", consumption_task_pipe),
    TaskPipeVisualisation("consumption", "consumption_task_pipe_negative", consumption_task_pipe_negative),
    TaskPipeVisualisation("setpoint_temperature", "setpoint_temperature", setpoint_temperature_task_pipe),
    TaskPipeVisualisation("working_rate", "working_rate", working_rate_task_pipe),
    TaskPipeVisualisation("occupancy_detection", "occupancy_detection", occupancy_detection_task_pipe),
]
extern_pipe_list = [
    TaskPipeVisualisation("main_pressure", "main_pressure", weather_builder_pipe()),
    TaskPipeVisualisation("main_humidity", "main_humidity", weather_builder_pipe()),
    TaskPipeVisualisation("main_temp", "main_temp", weather_builder_pipe()),
    TaskPipeVisualisation("main_temp_mini", "main_temp_mini", weather_builder_pipe()),
    TaskPipeVisualisation("main_temp_max", "main_temp_max", weather_builder_pipe()),
    TaskPipeVisualisation("wind_speed", "wind_speed", weather_builder_pipe()),
    TaskPipeVisualisation("exterior_temperature", "exterior_temperature", weather_builder_pipe())
]

class TaskMergePipe:
    def run(self, feature, output_table_name):
        first_name = feature[0].output_name
        first_builder = QueryBuilder()
        first_builder.add_select(" timestamp AS all_timestamp, device_url AS all_device, value AS " + first_name)
        first_builder.add_from_name(EasyTableName.get_pretty_output_visualisation(first_name))
        current_all_name = []
        current_all_name.append(first_name)
        old_big_builder = first_builder
        big_builder = QueryBuilder()
        for pipe in feature[1:]:
            name = pipe.output_name
            big_builder.add_select("IFNULL(" + name + ".timestamp, all_timestamp) AS all_timestamp, IFNULL(" + name + ".device_url, all_device) AS all_device, " + name + ".value AS " + name + " , " + ", ".join(current_all_name))
            big_builder.add_from(old_big_builder.get())
            big_builder.add("FULL OUTER JOIN " + pipe.load_task_parameter_list[-1].get_output() + " AS " + name)
            big_builder.add("ON all_timestamp = " + name + ".timestamp AND all_device = " + name + ".device_url")
            current_all_name.append(name)
            old_big_builder = big_builder.copy()
            big_builder = QueryBuilder()

        big_builder.add_create_replace(output_table_name)
        big_builder.add(old_big_builder.get())
        big_builder.get_execute()