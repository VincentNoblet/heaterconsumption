import dash_core_components as dcc, dash_html_components as html
from dash.dependencies import Output, Input, State
import dash_ui as dui
import plotly.graph_objs as go
import numpy as np

from pandas_task_static_historical import ScoreAnalyzer, RandomForestRegressor


class ParameterInput:
    def __init__(self, max_feature):
        self.hyper_parameter_input_element_list = [
                dcc.Dropdown(id="criterion", value="mse", options=[{"value": "mse", "label": "mse"}], style={"font-size" : "15px"}),
                dcc.Slider(id="max_depth", min=2, max=50, step=1, value=30),
                dcc.Slider(id="max_features", min=2, max=min(30, max_feature - 1), step=1, value=max_feature - 1),
                dcc.Slider(id="min_samples_split", min=2, max=100, step=3, value=10),
                dcc.Slider(id="n_estimators", min=10, max=200, step=10, value=30),
        ]

        self.hyper_parameter_input_element_name_id = [el.__getattribute__("id") for el in self.hyper_parameter_input_element_list]
        self.hyper_parameter_input_element_value_id = [id + "_value" for id in self.hyper_parameter_input_element_name_id]

    def get(self):
        parameter_input_callback_input = [Input(component_id=id, component_property='value') for id in self.hyper_parameter_input_element_name_id]
        parameter_input_callback_output = [Output(component_id=id, component_property='children') for id in self.hyper_parameter_input_element_value_id]

        def update_slider_value(*args):
            return [name + ": " + str(value) for name, value in zip(self.hyper_parameter_input_element_name_id, list(args))]

        hyper_parameter_input_element = [html.Div([html.H5(id=dcc_input.__getattribute__("id") + "_value"), dcc_input]) for dcc_input in self.hyper_parameter_input_element_list]
        return {"e": hyper_parameter_input_element, "f": update_slider_value, "i": parameter_input_callback_input, "o": parameter_input_callback_output}


class TrainButton:
    def __init__(self, analyser):
        self.analyser = analyser
        self.hyper_parameter_input_element_name_id = ParameterInput(40).hyper_parameter_input_element_name_id

    def terminate_message(self, i, *args):
        print(i, args)
        model = RandomForestRegressor(**dict(zip(self.hyper_parameter_input_element_name_id, list(args))))
        self.analyser.fit(model)
        message_entropy = "Score moyen en Test du modèle: " + str(round(self.analyser.best_test_score, 2))
        message_accuracy = "Précision relative médiane: " + str(round(self.analyser.best_relative_loss, 2) * 100) + "%"
        return [message_entropy, message_accuracy]

    def get(self):
        parameter_input_callback_output = [Output(component_id='entropy_score', component_property='children'), Output(component_id='accuracy_score', component_property='children')]
        parameter_input_callback_input = [Input(component_id='train_button', component_property='n_clicks')]
        parameter_input_callback_state = [State(component_id=id, component_property="value") for id in self.hyper_parameter_input_element_name_id]

        train_button_element = html.Div([html.Button(id='train_button', n_clicks=0, children='Train', style={"font-size" : "20px"}), html.H3(id='entropy_score'), html.H3(id='accuracy_score')])

        return {"e": train_button_element, "f": self.terminate_message, "s": parameter_input_callback_state, "i": parameter_input_callback_input, "o": parameter_input_callback_output}
