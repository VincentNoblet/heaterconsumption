import dash_core_components as dcc, dash_html_components as html
from dash.dependencies import Output, Input, State
import plotly.graph_objs as go

class ConsumptionGraph:
    def __init__(self, data):
        self.data = data

    def get(self):
        consumption_graph_callback_output = Output(component_id='consumption_graph', component_property='figure')
        consumption_graph_callback_input = [Input(component_id='variable_name', component_property='value')]

        def plot_consumption_over_variable(input_value):
            return {
                "data": [go.Scatter(x=self.data[input_value], y=self.data.consumption, mode='markers', showlegend=False)],
                "layout": go.Layout(xaxis=dict(title=input_value), yaxis=dict(title='consumption (Wh/m2'), hovermode='closest')
            }

        variable = dcc.Dropdown(id='variable_name', value="comfort_temperature", options=[{"value": col, "label": col} for col in self.data.columns])
        _consumption_graph = dcc.Graph(id='consumption_graph', figure=plot_consumption_over_variable("comfort_temperature"))
        consumption_graph_element = html.Div([_consumption_graph, variable])
        return {"e": consumption_graph_element, "f": plot_consumption_over_variable, "i": consumption_graph_callback_input, "o": consumption_graph_callback_output}
