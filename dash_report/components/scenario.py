import dash_core_components as dcc, dash_html_components as html
from dash.dependencies import Output, Input, State
import dash_bootstrap_components as dbc
import numpy as np
from pandas_task_static_historical import ScoreAnalyzer
from scipy.ndimage.filters import gaussian_filter1d
import os, joblib, pandas as pd, plotly.graph_objs as go, plotly.express as p, dash_ui as dui


class PredictionGraph:
    def __init__(self, data):
        self.X = data.drop("consumption", axis=1)
        self.Y = data["consumption"]

    def get_predict_button(self):
        return {"e": html.Button(id='update_input_button', n_clicks=0, children='Predict', style={"font-size" : "20px"})}

    def get(self):
        prediction_graph_callback_output = Output(component_id='prediction_graph', component_property='figure')
        prediction_graph_callback_input = [Input(component_id="update_input_button", component_property='n_clicks'), Input(component_id="variable_dropdown", component_property='value')]
        prediction_graph_callback_state = [State(component_id=id, component_property='value') for id in InputSelector(self.X.columns).input_name_list_id]
        data = dict(x=[], y=[], type='scatter', connectgaps=True)
        layout = dict(xaxis=dict(title="variable"), yaxis=dict(title='Consumption (Wh/m2'), hovermode='closest')
        fig = go.Figure(go.Figure(data=data, layout=layout))
        _prediction_graph_element = dcc.Graph(id='prediction_graph', figure=fig)
        return {"e": _prediction_graph_element, "f": self.plot_prediction_over_variable, "s": prediction_graph_callback_state, "i": prediction_graph_callback_input, "o": prediction_graph_callback_output}

    def plot_prediction_over_variable(self, i, variable_name, *args):
        X0 = pd.Series(args, index=self.X.columns)
        model = ScoreAnalyzer().load_model()
        X_variable = self.X[variable_name].describe()
        print(X_variable)
        X_range = np.linspace(X_variable["min"], X_variable["max"], 200)
        y = []
        for x in X_range:
            X0[variable_name] = x
            y.append(model.predict([X0])[0])
        y = gaussian_filter1d(y, 10)

        data = dict(x=X_range, y=y, type='scatter', connectgaps=True)
        layout = dict(xaxis=dict(title=variable_name), yaxis=dict(title='Consumption (Wh/m2'), hovermode='closest')
        fig = go.Figure(data=data, layout=layout)
        #fig.add_trace(go.Scatter(name="Valeur réelle", x=[T0], y=[Y0], mode="markers+text", text=["(" + str(round(T0, 1)) + "°C, " + str(int(Y0 / 1000.)) + "kWh)"], textposition="bottom center"))
        #fig.add_trace(go.Scatter(name="Valeur prédite", x=[T0], y=[Y1], mode="markers+text", text=["(" + str(round(T0, 1)) + "°C, " + str(int(Y1 / 1000.)) + "kWh)"], textposition="bottom center"))
        return fig


class InputSelector:
    def __init__(self, input_columns):
        self.input_name_list = input_columns
        self.input_name_list_id = [id + "_input" for id in self.input_name_list]

    def get(self):
        col_count = 2
        col_size = 6
        all_dbc_row = []
        all_dbc_col = []
        for i, (name, id) in enumerate(zip(self.input_name_list, self.input_name_list_id)):
            col = i % col_count
            all_dbc_col.append(dbc.Col(html.H4([html.H4(name), dcc.Input(id=id, value=2)]), width=col_size))
            if col == 1:
                all_dbc_row.append(dbc.Row(all_dbc_col))
                all_dbc_col = []
        all_dbc_row.append(dbc.Row(all_dbc_col))
        return {"e": html.Div(all_dbc_row)}

    def get_input_state(self):
        return [Output(component_id=id, component_property='value') for id in self.input_name_list_id]
