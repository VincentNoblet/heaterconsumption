import dash_core_components as dcc
import dash_html_components as html
import dash_table
from components import Header, print_button
from datetime import datetime as dt
from datetime import date, timedelta
import pandas as pd
from pandas_task_static_historical import ScoreAnalyzer, RandomForestRegressor
analyser = ScoreAnalyzer()
model = RandomForestRegressor(
                criterion='mse',
                max_depth=20,
                max_features=7,
                min_samples_split=10,
                n_estimators=50)
analyser.fit(model)

# Read in Travel Report Data
df = analyser.data_prepared



