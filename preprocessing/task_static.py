from preprocessing.main_task import TaskBigQueryName
from manager.bigquery_structure import EasyTableName
from manager.database_updater import DbManager


class TaskCopyStatic(TaskBigQueryName):
    def draft_query(self, query_builder): return DbManager.select_static(query_builder, self.name)


class TaskUnifySetup(TaskBigQueryName):
    def requires(self): return TaskCopyStatic(self.name)

    def draft_query(self, query_builder):
        query_builder.add("SELECT * FROM " + self.input().load() + " AS setup")
        query_builder.add_full_outer_join("(SELECT tenement_build_date AS tenement_build_date_join, tenement_build_date_english FROM " + EasyTableName.get_utils_table_name("unify_tenement_build_date") + ")")
        query_builder.add("ON tenement_build_date_join = setup.tenement_build_date")
        query_builder.add_full_outer_join("(SELECT tenement_type, tenement_type_english FROM " + EasyTableName.get_utils_table_name("unify_tenement_type") + ")")
        query_builder.add("ON tenement_type = tenement_type_label")
        return query_builder


class TaskUnifyRoom(TaskBigQueryName):
    def requires(self): return TaskCopyStatic(self.name)

    def draft_query(self, query_builder):
        query_builder.add("SELECT * FROM " + self.input().load() + " AS room")
        query_builder.add_full_outer_join(EasyTableName.get_utils_table_name("unify_room_type") + " AS unify_room")
        query_builder.add("ON CAST(room.type_id AS INT64) = unify_room.room_type_id")
        query_builder.add_where("type_id != '0' AND type_id != '200' AND type_id != '201'")
        return query_builder



