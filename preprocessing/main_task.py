import luigi
from config import d6tflow
from manager.database_updater import DbManager
from manager.query_customize import QueryBuilder
from manager.query_customize import EasyQuery
from manager.bigquery_structure import EasyTableName
from preprocessing.pipe_task import TaskStatePipe


class TaskBigQuery(d6tflow.tasks.TaskPickle):
    def __init__(self, *args, **kwargs):
        super(TaskBigQuery, self).__init__(*args, **kwargs)
        self.input_table = None
        self.dependency = None
        self.extra_dependency = None

    def set_dependency(self, dependency):
        if self.extra_dependency is not None:
            self.dependency = {'main': dependency, 'extra': self.extra_dependency}
        else:
            self.dependency = dependency

    def requires(self): return self.dependency

    def get_unique_hash_id(self):
        return EasyTableName.get_consumption_dev(self.name + "_" + self.task_id)

    def draft_query(self, query_builder): pass

    def run(self):
        output = self.get_unique_hash_id()
        query_builder = QueryBuilder()
        query_builder.add_create_replace(output)
        query_builder = self.draft_query(query_builder)
        query_builder.execute()
        self.save(output)


class TaskBigQueryName(TaskBigQuery):
    name = luigi.parameter.Parameter()


class TaskGetState(TaskBigQueryName):
    def draft_query(self, query_builder): return DbManager.select_historical(query_builder, self.name)


class TaskGetRawdevicestateConsumption(TaskBigQueryName):
    def draft_query(self, query_builder): return DbManager.select_rawdevicestate(query_builder)


class TaskGetOpenWeathermap(TaskBigQueryName):
    def draft_query(self, query_builder): return DbManager.select_openweathermap(query_builder, self.name)


class TaskDiffValue(TaskBigQueryName):
    def draft_query(self, diff_value_builder):
        diff_value_builder.add_select("device_url, timestamp, IFNULL(LEAD(value) OVER (PARTITION BY device_url ORDER BY timestamp ASC) - value, 0) AS value")
        diff_value_builder.add_from_name(self.input().load())
        return diff_value_builder


class TaskLeadOverTimestampEndTimestamp(TaskBigQueryName):
    def draft_query(self, lead_over_builder):
        first_field = EasyQuery.get_field(self.input().load())[0]
        lead_over_builder.add_select(first_field + ", timestamp, value, LEAD(timestamp) OVER (PARTITION BY " + first_field + " ORDER BY timestamp ASC) AS end_timestamp, TIMESTAMP_DIFF(LEAD(timestamp) OVER (PARTITION BY " + first_field + " ORDER BY timestamp ASC), timestamp, MINUTE) AS period")
        lead_over_builder.add_from_name(self.input().load())
        return lead_over_builder


class TaskLeadOverTimestamp(TaskBigQueryName):
    def draft_query(self, lead_over_builder):
        first_field = EasyQuery.get_field(self.input().load())[0]
        lead_over_builder.add_select(first_field + ", timestamp, value, TIMESTAMP_DIFF(LEAD(timestamp) OVER (PARTITION BY " + first_field + " ORDER BY timestamp ASC), timestamp, MINUTE) AS period")
        lead_over_builder.add_from_name(self.input().load())
        return lead_over_builder


class TaskLeadOverValue(TaskBigQueryName):
    def draft_query(self, lead_over_builder):
        first_field = EasyQuery.get_field(self.input().load())[0]
        lead_over_builder.add_select(first_field + ", timestamp, LEAD(value) OVER (PARTITION BY " + first_field + " ORDER BY timestamp ASC) - value AS value")
        lead_over_builder.add_from_name(self.input().load())
        return lead_over_builder


class TaskCountDistinctValue(TaskBigQueryName):
    def draft_query(self, query_builder):
        query_builder.add_select("device_url, year, value, count(*) AS w")
        query_builder.add_from_name(self.input().load())
        query_builder.add_group_by("device_url, year, value")
        return query_builder


class TaskLabelWeightYear(TaskBigQueryName):
    partition = luigi.parameter.Parameter()

    def draft_query(self, delete_null):
        gateway_year_label_builder = QueryBuilder()
        gateway_year_label_builder.add_select("device_url, " + EasyQuery.get_year_from_timestamp(dict(self.partition), "timestamp") + ", value, w")
        gateway_year_label_builder.add_from_name(self.input().load())
        delete_null.add_select_all()
        delete_null.add_from(gateway_year_label_builder.get())
        delete_null.add_where("year IS NOT NULL")
        return delete_null


class TaskWeightAverage(TaskBigQueryName):
    def draft_query(self, query_builder):
        query_builder.add_select("device_url, year, SUM(w * value) / SUM(w) AS value")
        query_builder.add_from_name(self.input().load())
        query_builder.add_group_by("device_url, year")
        return query_builder


class TaskLeadOverGatewayTimestamp(TaskBigQueryName):
    def draft_query(self, lead_over_builder):
        lag_builder = QueryBuilder()
        lag_builder.add_select("gateway_id, timestamp, LEAD(timestamp) OVER (PARTITION BY gateway_id ORDER BY timestamp ASC) AS end_timestamp, value")
        lag_builder.add_from_name(self.input().load())

        lead_over_builder.add_select("gateway_id, timestamp, end_timestamp, TIMESTAMP_DIFF(end_timestamp, timestamp , MINUTE) AS period, value")
        lead_over_builder.add_from(lag_builder.get())
        return lead_over_builder


class TaskRoguePeriod(TaskBigQueryName):
    period_max = luigi.parameter.IntParameter()

    def draft_query(self, rogue_period_builder):
        rogue_period_builder.add_select_all()
        rogue_period_builder.add_from_name(self.input().load())
        rogue_period_builder.add("WHERE period <= " + str(self.period_max) + " AND period <> 0")
        return rogue_period_builder


class TaskRogueValueInterval(TaskBigQueryName):
    value_min = luigi.parameter.IntParameter()
    value_max = luigi.parameter.IntParameter()

    def draft_query(self, rogue_value_builder):
        rogue_value_builder.add_select_all()
        rogue_value_builder.add_from_name(self.input().load())
        rogue_value_builder.add("WHERE value <= " + str(self.value_max) + " AND value >= " + str(self.value_min))
        return rogue_value_builder


class TaskFilterComfortRange(TaskBigQueryName):
    def __init__(self, *args, **kwargs):
        super(TaskFilterComfortRange, self).__init__(*args, **kwargs)
        pipe = TaskStatePipe(input_name='comfort_temperature', output_name='comfort_range_dependency', input_list=[(TaskGetState, {})], output_list=[
            (TaskLeadOverTimestampEndTimestamp, {}),
            (TaskRoguePeriod, {"period_max": 1500}),
            (TaskRogueValueInterval, {"value_min": 12, "value_max": 28})])
        self.extra_dependency = pipe.get_last_task()
    def draft_query(self, query_builder):
        query_builder.add_select("SETPOINT.device_url AS device_url, SETPOINT.timestamp AS timestamp, SETPOINT.period AS value")
        query_builder.add_from_name(self.input()['main'].load())
        query_builder.add("AS SETPOINT")
        query_builder.add("INNER JOIN")
        print(self.input()['extra'].load())
        query_builder.add(self.input()['extra'].load())
        query_builder.add("AS COMFORT")
        query_builder.add("ON SETPOINT.device_url = COMFORT.device_url AND COMFORT.timestamp <= SETPOINT.timestamp AND COMFORT.end_timestamp >= SETPOINT.timestamp")
        query_builder.add("AND ABS(SETPOINT.value - COMFORT.value) <= 1")
        return query_builder


class TaskAbsolute(TaskBigQueryName):
    def draft_query(self, query_builder):
        query_builder.add_select("device_url, timestamp, ABS(value) AS value")
        query_builder.add_from_name(self.input().load())
        return query_builder


class TaskRemoveDoublonValue(TaskBigQueryName):
    def draft_query(self, doublon_builder):
        query_builder = QueryBuilder()
        query_builder.add_select("device_url, timestamp, LAG(value) OVER (PARTITION BY device_url ORDER BY timestamp ASC) AS previous_value, value")
        query_builder.add_from_name(self.input().load())
        doublon_builder.add_select("device_url, timestamp, value")
        doublon_builder.add_from(query_builder.get())
        doublon_builder.add("WHERE previous_value <> value")
        return doublon_builder


class TaskSetWorkingRateStandByAfterDoublonLeadOver(TaskBigQueryName):
    def draft_query(self, zero_standby_builder):
        zero_standby_builder.add_select("device_url, timestamp, period, CASE WHEN value=100 AND period > 250 THEN 0 ELSE value END AS value")
        zero_standby_builder.add_from_name(self.input().load())
        return zero_standby_builder


class TaskSetWorkingRateStandBy(TaskBigQueryName):

    def draft_query(self, zero_standby_builder):
        zero_standby_builder.add_select_all()
        zero_standby_builder.add_from_name(self.input().load())
        zero_standby_builder.add("WHERE NOT(value = 100 and period >= 200)")
        return zero_standby_builder


class TaskFilterValue(TaskBigQueryName):
    value = luigi.parameter.IntParameter()

    def draft_query(self, filter_builder):
        filter_builder.add_select_all()
        filter_builder.add_from_name(self.input().load())
        filter_builder.add_where("value = '" + self.value + "'")
        return filter_builder


class TaskRoundTimestamp(TaskBigQueryName):
    timestamp_inverval = luigi.parameter.IntParameter()

    def draft_query(self, round_builder):
        minute = str(self.timestamp_inverval)
        round_builder.add_select("TIMESTAMP_SECONDS(" + minute + "*60 * DIV(UNIX_SECONDS(timestamp), " + minute + " * 60)) AS timestamp_round, device_url, timestamp ")
        round_builder.add_from_name(self.input().load())
        return round_builder


class TaskSelectDistinctTimestampRoundDeviceYear(TaskBigQueryName):
    def draft_query(self, query_builder):
        query_builder.add_select("DISTINCT timestamp_round, device_url, year")
        query_builder.add_from_name(self.input().load())
        return query_builder


class TaskGenerateTimestampArray(TaskBigQueryName):
    timestamp_inverval = luigi.parameter.IntParameter()

    def draft_query(self, query_builder):
        array_builder = QueryBuilder()
        minute = str(self.timestamp_inverval)
        array_builder.add_select("GENERATE_TIMESTAMP_ARRAY(" + EasyQuery.get_round_timestamp_minute("timestamp", minute) + ", "
                                 + EasyQuery.get_round_timestamp_minute("end_timestamp", minute) + ", "
                                 "INTERVAL " + minute + " MINUTE) AS intermediate_timestamp, timestamp, end_timestamp, device_url"
                                 )
        array_builder.add_from_name(self.input().load())

        query_builder.add_select(
        " device_url, intermediate_timestamp_flatten AS timestamp_round, timestamp, CASE" +
            " WHEN " + EasyQuery.get_round_timestamp_minute("end_timestamp", minute) + " = " + EasyQuery.get_round_timestamp_minute("timestamp", minute) +
            " THEN TIMESTAMP_DIFF(end_timestamp, timestamp, MINUTE) " +
            " WHEN intermediate_timestamp_flatten = " + EasyQuery.get_round_timestamp_minute("end_timestamp", minute) +
            " THEN TIMESTAMP_DIFF(end_timestamp, intermediate_timestamp_flatten, MINUTE) " +
            " WHEN intermediate_timestamp_flatten = " + EasyQuery.get_round_timestamp_minute("timestamp", minute) +
            " THEN (" + minute + " - TIMESTAMP_DIFF(timestamp, intermediate_timestamp_flatten, MINUTE)) " +
            " ELSE " + minute +
            " END AS value")
        query_builder.add_from(array_builder.get())
        query_builder.add("CROSS JOIN UNNEST((intermediate_timestamp)) AS intermediate_timestamp_flatten")
        return query_builder


class TaskSumDeviceTimestampRound(TaskBigQueryName):
    timestamp_inverval = luigi.parameter.IntParameter()

    def draft_query(self, sum_value_builder):
        minutes = str(self.timestamp_inverval)
        sum_value_builder.add_select("device_url, timestamp_round AS timestamp, SUM(value) / " + minutes + " AS value")
        sum_value_builder.add_from_name(self.input().load())
        sum_value_builder.add_group_by("device_url, timestamp")
        return sum_value_builder


class TaskLabelDay(TaskBigQueryName):
    def draft_query(self, label_year_builder):
        label_year_builder.add_select("gateway_id, TIMESTAMP(EXTRACT(DATE FROM TIMESTAMP)) AS timestamp, value")
        label_year_builder.add_from_name(self.input().load())
        return label_year_builder


class TaskMinDay(TaskBigQueryName):
    def draft_query(self, min_builder):
        min_builder.add_select("gateway_id, timestamp, MIN(value) AS value")
        min_builder.add_from_name(self.input().load())
        min_builder.add_group_by("gateway_id, timestamp")
        return min_builder


class TaskLabelYear(TaskBigQueryName):
    partition = luigi.parameter.Parameter()

    def draft_query(self, delete_null):
        all_field = EasyQuery.get_field(self.input().load())
        select_field = [all_field[0], EasyQuery.get_year_from_timestamp(dict(self.partition), "timestamp"), "value"]
        if "period" in all_field:
            select_field.append("period as w")
        label_year_builder = QueryBuilder()
        label_year_builder.add_select_from_list(select_field)
        label_year_builder.add_from_name(self.input().load())
        delete_null.add_select_all()
        delete_null.add_from(label_year_builder.get())
        delete_null.add_where("year IS NOT NULL")
        return delete_null


class TaskSumYear(TaskBigQueryName):
    def draft_query(self, sum_year_builder):
        first_two_field = ", ".join(EasyQuery.get_field(self.input().load())[:2])
        sum_year_builder.add_select(first_two_field + ", SUM(value) AS value")
        sum_year_builder.add_from_name(self.input().load())
        sum_year_builder.add_group_by(first_two_field)
        return sum_year_builder


class TaskSumGateway(TaskBigQueryName):
    def draft_query(self, sum_year_builder): return TaskSumYear(self.name).draft_query(sum_year_builder)


class TaskAverageYear(TaskBigQueryName):
    def draft_query(self, sum_year_builder):
        first_field = EasyQuery.get_field(self.input().load())[0]
        sum_year_builder.add_select(first_field + ", year, AVG(value) AS value")
        sum_year_builder.add_from_name(self.input().load())
        sum_year_builder.add_group_by(first_field + ", year")
        return sum_year_builder


class TaskWeightAverageYear(TaskBigQueryName):
    def draft_query(self, sum_year_builder):
        first_field = EasyQuery.get_field(self.input().load())[0]
        sum_year_builder.add_select(first_field + ", year, SUM(value * w) / SUM(w) AS value")
        sum_year_builder.add_from_name(self.input().load())
        sum_year_builder.add_group_by(first_field + ", year")
        return sum_year_builder


class TaskLabelGateway(TaskBigQueryName):
    def draft_query(self, average_value_builder):
        average_value_builder.add_select(EasyQuery.get_gateway_from_device() + " , year, value")
        average_value_builder.add_from_name(self.input().load())
        return average_value_builder


class TaskAverageGateway(TaskBigQueryName):
    def draft_query(self, average_value_builder):
        average_value_builder.add_select("gateway_id, year, AVG(value) AS value")
        average_value_builder.add_from_name(self.input().load())
        average_value_builder.add_group_by("gateway_id, year")
        return average_value_builder


class TaskRename(TaskBigQueryName):
    input_field = luigi.parameter.ListParameter()
    output_field = luigi.parameter.ListParameter()

    def draft_query(self, rename_builder):
        input_as_output = ", ".join(list(map(lambda x: " AS ".join(x), list(zip(self.input_field, self.output_field)))))
        rename_builder.add_select(input_as_output)
        rename_builder.add_from_name(self.input().load())
        return rename_builder


class TaskRatio(TaskBigQueryName):
    denominator = luigi.parameter.IntParameter()

    def draft_query(self, average_value_builder):
        average_value_builder.add_select("device_url, year, SUM(value) /" + str(self.denominator) + " AS value")
        average_value_builder.add_from_name(self.input().load())
        average_value_builder.add_group_by("device_url, year")
        return average_value_builder


class TaskRemoveOpenWeatherMapDoublon(TaskBigQueryName):

    def draft_query(self, query_builder):
        query_builder.add_select_all()
        query_builder.add_from_name(self.input().load())
        query_builder.add("WHERE period <> 0")
        return query_builder
