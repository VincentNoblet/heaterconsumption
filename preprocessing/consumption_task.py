import luigi
from preprocessing.main_task import TaskBigQueryName
from manager.query_customize import QueryBuilder
from manager.query_customize import EasyQuery


class TaskLeadOverTimestamp(TaskBigQueryName):
    def draft_query(self, lead_over_builder):
        first_field = EasyQuery.get_field(self.input().load())[0]
        temp = QueryBuilder()
        temp.add_select("*, LEAD(timestamp) OVER (PARTITION BY " + first_field + " ORDER BY timestamp ASC) AS end_timestamp")
        temp.add_from_name(self.input().load())
        lead_over_builder.add_select("*, TIMESTAMP_DIFF(end_timestamp, timestamp, MINUTE) AS period")
        lead_over_builder.add_from(temp.get())
        return lead_over_builder


class TaskRogueValueInterval(TaskBigQueryName):
    value_min = luigi.parameter.IntParameter()
    value_max = luigi.parameter.IntParameter()

    def draft_query(self, rogue_value_builder):
        value_max_by_minute = str(self.value_max/1440)
        rogue_value_builder.add_select("device_url, timestamp, value")
        rogue_value_builder.add_from_name(self.input().load())
        rogue_value_builder.add("WHERE value >= " + str(self.value_min) + " AND ((period > 1500 AND value/period <= " + value_max_by_minute + ") OR (period <= 1500 AND value < " + str(self.value_max) + "))")
        return rogue_value_builder


class TaskCorrectSpanValue(TaskBigQueryName):
    partition = luigi.parameter.Parameter()

    def draft_query(self, correct_span_builder):
        correct_span_builder.add_select("device_url, timestamp, " + EasyQuery.case_then_span_right_left(dict(self.partition)) + ", period")
        correct_span_builder.add_from_name(self.input().load())
        return correct_span_builder


class TaskLabelGatewayYear(TaskBigQueryName):
    partition = luigi.parameter.Parameter()

    def draft_query(self, delete_null):
        gateway_year_label_builder = QueryBuilder()
        gateway_year_label_builder.add_select(EasyQuery.get_gateway_from_device() + ", " + EasyQuery.get_year_from_begin_end_timestamp(dict(self.partition)) + ", value")
        gateway_year_label_builder.add_from_name(self.input().load())
        delete_null.add_select_all()
        delete_null.add_from(gateway_year_label_builder.get())
        delete_null.add_where("year IS NOT NULL")
        return delete_null
