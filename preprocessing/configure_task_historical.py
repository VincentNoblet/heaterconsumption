from preprocessing.main_task import *
from preprocessing.pipe_task import *
from preprocessing import consumption_task
from config import cfg

cfg_variable = cfg['variable']
partition_winter = cfg_variable['partition_winter']
partition_year = cfg_variable['partition_year']
winter_duration_minute = max(map(lambda x: x['end_date'] - x['begin_date'], partition_winter.values())).days * 24 * 60

pipe_list = [
#DATAFLOW
TaskStatePipe(input_name='rawdevicestate_consumption', output_name='dataflow', input_list=[
    (TaskGetRawdevicestateConsumption, {}),
    (TaskLeadOverTimestamp, {}),
    (TaskRoguePeriod, {"period_max": 250}),
    ], output_list=[
    (TaskRename, {"input_field": ["device_url", "timestamp", "period"], "output_field": ["device_url", "timestamp", "value"]}),
    (TaskLabelYear, {"partition": partition_winter}),
    (TaskSumYear, {}),
    (TaskRatio, {"denominator": winter_duration_minute}),
    (TaskLabelGateway, {}),
    (TaskAverageGateway, {}),
]),
#AMBIENT
TaskStatePipe(input_name='ambient_temperature', output_name='ambient_temperature', input_list=[
    (TaskGetState, {}),
    (TaskLeadOverTimestamp, {}),
    (TaskRoguePeriod, {"period_max": 250}),
    (TaskRogueValueInterval, {"value_min": 0, "value_max": 40})
    ], output_list=[
    (TaskLabelYear, {"partition": partition_winter}),
    (TaskWeightAverageYear, {}),
    (TaskLabelGateway, {}),
    (TaskAverageGateway, {}),
]),
#WORKING_RATE
TaskStatePipe(input_name='working_rate', output_name='working_rate', input_list=[
    (TaskGetState, {}),
    (TaskLeadOverTimestamp, {}),
    (TaskRoguePeriod, {"period_max": 250}),
    (TaskRogueValueInterval, {"value_min": 0, "value_max": 100})
    ], output_list=[
    (TaskLabelYear, {"partition": partition_winter}),
    (TaskWeightAverageYear, {}),
    (TaskLabelGateway, {}),
    (TaskAverageGateway, {}),
]),
#COMFORT
TaskStatePipe(input_name='comfort_temperature', output_name='comfort_temperature', input_list=[
    (TaskGetState, {}),
    (TaskLeadOverTimestamp, {}),
    (TaskRoguePeriod, {"period_max": 1500}),
    (TaskRogueValueInterval, {"value_min": cfg_variable['comfort_temperature_high'], "value_max": 28})
    ], output_list=[
    (TaskLabelYear, {"partition": partition_winter}),
    (TaskSumYear, {}),
    (TaskRatio, {"denominator": winter_duration_minute}),
    (TaskLabelGateway, {}),
    (TaskAverageGateway, {}),
]),
#SETPOINT
TaskStatePipe(input_name='setpoint_temperature', output_name='setpoint_temperature', input_list=[
    (TaskGetState, {}),
    (TaskLeadOverTimestamp, {}),
    (TaskRoguePeriod, {"period_max": 1500}),
    (TaskRogueValueInterval, {"value_min": 12, "value_max": 28}),
    ], output_list=[
    (TaskLabelYear, {"partition": partition_winter}),
    (TaskSumYear, {}),
    (TaskRatio, {"denominator": winter_duration_minute}),
    (TaskLabelGateway, {}),
    (TaskAverageGateway, {}),
]),
#COMFORT_TIME
TaskStatePipe(input_name='setpoint_temperature', output_name='comfort_time_temperature', input_list=[
    (TaskGetState, {}),
    (TaskLeadOverTimestamp, {}),
    (TaskRoguePeriod, {"period_max": 1500}),
    (TaskRogueValueInterval, {"value_min": cfg_variable['comfort_temperature_time'], "value_max": 28}),
], output_list=[
    (TaskRename, {"input_field": ["device_url", "timestamp", "period"], "output_field": ["device_url", "timestamp", "value"]}),
    (TaskLabelYear, {"partition": partition_winter}),
    (TaskSumYear, {}),
    (TaskRatio, {"denominator": winter_duration_minute}),
    (TaskLabelGateway, {}),
    (TaskAverageGateway, {}),
]),
TaskStatePipe(input_name='setpoint_temperature', output_name='comfort_range_time', input_list=[
    (TaskGetState, {}),
    (TaskLeadOverTimestamp, {}),
    (TaskRoguePeriod, {"period_max": 1500}),
], output_list=[
    (TaskFilterComfortRange, {}),
    (TaskLabelYear, {"partition": partition_winter}),
    (TaskSumYear, {}),
    (TaskRatio, {"denominator": winter_duration_minute}),
    (TaskLabelGateway, {}),
    (TaskSumGateway, {}),
]),
#CONSUMPTION
TaskStatePipe(input_name='consumption_index', output_name='consumption_index', input_list=[
    (TaskGetState, {}),
    (TaskDiffValue, {}),
    (TaskLeadOverTimestamp, {}),
    (consumption_task.TaskRogueValueInterval, {"value_min": 0, "value_max": 48000})
], output_list=[
    (TaskLabelYear, {"partition": partition_winter}),
    (TaskSumYear, {}),
    (TaskLabelGateway, {}),
    (TaskSumGateway, {})
]),
TaskStatePipe(input_name='consumption_index', output_name='consumption_index_year', input_list=[
    (TaskGetState, {}),
    (TaskDiffValue, {}),
    (TaskLeadOverTimestamp, {}),
    (consumption_task.TaskRogueValueInterval, {"value_min": 0, "value_max": 48000})
], output_list=[
    (TaskLabelYear, {"partition": partition_year}),
    (TaskSumYear, {}),
    (TaskLabelGateway, {}),
    (TaskSumGateway, {})
]),
#LOWERING
TaskStatePipe(input_name='setpoint_temperature', output_name='lowering_temperature_virtual', input_list=[
    (TaskGetState, {}),
], output_list=[
    (TaskLeadOverValue, {}),
    (TaskAbsolute, {}),
    (TaskRogueValueInterval, {"value_min": 2.5, "value_max": 9}),
    (TaskLabelYear, {'partition': partition_winter}),
    (TaskCountDistinctValue, {}),
    (TaskWeightAverage, {}),
    (TaskLabelGateway, {}),
    (TaskAverageGateway, {})
]),
#LOWERING
TaskStatePipe(input_name='setpoint_temperature', output_name='lowering_temperature_virtual', input_list=[
    (TaskGetState, {}),
], output_list=[
    (TaskLeadOverValue, {}),
    (TaskAbsolute, {}),
    (TaskRogueValueInterval, {"value_min": 2.5, "value_max": 9}),
    (TaskLabelYear, {'partition': partition_winter}),
    (TaskCountDistinctValue, {}),
    (TaskWeightAverage, {}),
    (TaskLabelGateway, {}),
    (TaskAverageGateway, {})
]),
#MAIN TEMP
TaskStatePipe(input_name='main_temp', output_name='main_temp', input_list=[
    (TaskGetOpenWeathermap, {}),
    (TaskLeadOverTimestamp, {}),
    (TaskRemoveOpenWeatherMapDoublon, {}),
], output_list=[
    (TaskLabelYear, {"partition": partition_winter}),
    (TaskAverageYear, {})
]),
#MODE
TaskStatePipe(input_name='operating_mode', output_name='standby', input_list=[
    (TaskGetState, {}),
    (TaskLeadOverTimestamp, {}),
], output_list=[
    (TaskFilterValue, {'value': 'standby'}),
    (TaskRename, {"input_field": ["device_url", "timestamp", "period"], "output_field": ["device_url", "timestamp", "value"]}),
    (TaskLabelYear, {"partition": partition_winter}),
    (TaskSumYear, {}),
    (TaskRatio, {"denominator": winter_duration_minute}),
    (TaskLabelGateway, {}),
    (TaskAverageGateway, {})
])]
pipe_dict = dict(zip(map(lambda pipe: pipe.output_name, pipe_list), pipe_list))
