#!/bin/bash
echo "This programm convert historical variable to parquet format given a variable name and a year. PQ files are stored in the data folders"
read -p "Variable to be saved? " variable_name
read -p "Year to be saved? " year
execute() {
    ../venv/bin/python historical_to_parquet.py $variable_name $year
}
execute

