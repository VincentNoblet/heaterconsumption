import datetime
from pathlib import Path
import os
import inspect


class PQNodeFileName:
    def __init__(self, name, date):
        self.name = name
        self.date = date

    def get(self):
        parquet_path = Path(inspect.getfile(PQNodeFileName)).parent
        return parquet_path.joinpath("parquet_file/static").joinpath(self.name + "_" + str(self.date) + ".pq")


class TaskSaveStatic:

    def __init__(self, name):
        self.name = name

    def run(self, result):
        saving_path = Path(PQNodeFileName(self.name, str(datetime.datetime.now().date())).get())
        if not saving_path.exists():
            frame = result.to_dataframe()
            frame.to_parquet(saving_path)
