@ECHO OFF
ECHO This programm convert historical variable to parquet format given a variable name and a year. PQ files are stored in the data folder
set /p variable=Variable to be saved:
set /p year=Year to be saved:
"venv/Scripts/python.exe" historical_to_parquet.py %variable% %year%