#Convert ambient_temperature data to parquet
###Linux
./venv/bin/python parquet/historical_to_parquet.py ambient_temperature 2016-6-14 2016-7-14
./venv/bin/python parquet/historical_to_parquet.py ambient_temperature 2016
###Windows
"venv/Scripts/python.exe" parquet/historical_to_parquet.py ambient_temperature 2016-6-14 2016-7-14
"venv/Scripts/python.exe" parquet/historical_to_parquet.py ambient_temperature 2016
