# Import SparkSession
from pyspark.sql import SparkSession
from pyspark.sql.dataframe import DataFrame
import manager
from pyspark.sql import SQLContext
from pyspark.sql.functions import udf
import pyspark.sql.types
def transform(self, f):
    return f(self)
DataFrame.transform = transform

# Build the SparkSession
spark = SparkSession.builder \
    .master("local") \
    .appName("Linear Regression Model") \
    .config("spark.executor.memory", "1gb") \
    .getOrCreate()

sc = spark.sparkContext

import os
import re
from parquet.historical_to_parquet import TaskQueryHistorical
from manager.bigquery_structure import EasyTableName
import datetime
from pathlib import Path


class TaskMergeParquet:
    linux_remote_path = "/run/media/superjazz/3a55fb9b-2cc1-4b6c-acb4-1fea4cfe7970/Backup/consumption_parquet"
    local_path = Path("parquet_file/historical")
    windows_remote_path = "D:/vnoblet/consumption_parquet/"

    def run(self):
        data_file_list = list(os.listdir(self.windows_remote_path))
        for file in data_file_list[10:]:
            parquetFile = spark.read.parquet(self.windows_remote_path + file)
            parquetFile.show()


#comfort = spark.read.parquet("D:/vnoblet/consumption_parquet/comfort_temperature*.pq")

def with_date(df, transform_date):
    return df.withColumn("date", transform_date)

#comfort = comfort.drop("parameter_oid")
#comfort = comfort.withColumn("year", pyspark.sql.functions.year(comfort.timestamp))
#comfort = comfort.drop(comfort.timestamp)

for name in manager.bigquery_structure.EasyTableName.historical_table_name_list:
    print(spark.read.parquet("D:/vnoblet/consumption_parquet/" + name + "*.pq"))