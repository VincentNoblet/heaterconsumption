from manager.query_customize import QueryBuilder, EasyQuery
from manager.bigquery_structure import EasyTableName
from config import cfg
weather = cfg['bigquery']['weather']
variable_cfg = cfg['variable']


class DbManager:
    electrical_heater_url = EasyTableName.get_dev_table_name("electrical_heater_url")
    rawdevicestate_consumption = EasyTableName.get_consumption("rawdevicestate_consumption")
    openweathermap = EasyTableName.get_consumption("openweathermap")

    @staticmethod
    def create_electrical_heater_filter():
        query_builder = QueryBuilder()
        query_builder.add_create_replace(DbManager.electrical_heater_url_name)
        query_builder.add_select("distinct(url_device) AS device_url")
        query_builder.add_from_name(EasyTableName.get_bucket("device"))
        query_builder.add("AS device")
        query_builder.add_full_outer_join(EasyTableName.get_bucket("heater"))
        query_builder.add("AS heater")
        query_builder.add("ON device.id = heater.id")
        query_builder.get_execute()

    @staticmethod
    def create_rawdevicestate():
        query_builder = QueryBuilder()
        query_builder.add_create_replace(DbManager.rawdevicestate_consumption)
        query_builder.add_select_historical_field_parameter_oid()

        query_builder_rawdevicestate_convert = QueryBuilder()
        query_builder_rawdevicestate_convert.add_select("CONCAT('io://', gateway_id, '/', CAST(device_address AS STRING)) AS device_url, CAST(parameter_oid AS INT64) AS parameter_oid, timestamp AS timestamp, value AS value")
        query_builder_rawdevicestate_convert.add_from_name(EasyTableName.get_bucket("rawdevicestate"))

        query_builder.add_from(query_builder_rawdevicestate_convert.get())
        query_builder.add_where("device_url IN (SELECT * FROM " + DbManager.electrical_heater_url + ")")
        query_builder.execute()


    @staticmethod
    def create_openweathermap():
        openweathermap_cfg = weather['openweathermap']
        openweathermap_history_cfg = weather['openweathermap_history']
        openweathermap_table, openweathermap_history_table = EasyTableName.get_openweathermap(), EasyTableName.get_openweathermap_history()
        openweathermap_field, openweathermap_history_field = EasyQuery.get_field(openweathermap_table), EasyQuery.get_field(openweathermap_history_table)
        field_to_replace = "ID_Station"
        join_list = list(set(openweathermap_field) & set(openweathermap_history_field))
        if openweathermap_cfg["ID_Station"] in openweathermap_field:
            join_list += ["ID_Station"]
            field_to_replace += " AS " + openweathermap_cfg["ID_Station"]
        openweathermap_field, openweathermap_history_field = join_list.copy(), join_list.copy()
        openweathermap_field[openweathermap_field.index("ID_Station")] = field_to_replace

        openweathermap_field[openweathermap_field.index('sys_message')] = 'CAST(sys_message AS ' + openweathermap_cfg['sys_message'] + ") AS sys_message"
        openweathermap_history_field[openweathermap_history_field.index('main_pressure')] = 'CAST(main_pressure AS ' + openweathermap_history_cfg['main_pressure'] + ") AS main_pressure"

        join_list[join_list.index('ID_Station')] = "setup.ID_Station AS Station_ID"
        union_builder = QueryBuilder()
        union_builder.add(EasyQuery.get_fast_select(openweathermap_table, openweathermap_field))
        union_builder.add_union_all()
        union_builder.add(EasyQuery.get_fast_select(openweathermap_history_table, openweathermap_history_field))

        query_builder = QueryBuilder()
        query_builder.add_create_replace(DbManager.openweathermap)
        query_builder.add_select("setup.gateway_id AS gateway_id, dt AS timestamp, " + ", ".join(join_list))
        query_builder.add_from(union_builder.get())

        query_builder.add("AS exterior_temperature")
        query_builder.add("JOIN (SELECT gateway_id, ID_Station FROM " + EasyTableName.get_bucket("setup") + ")")
        query_builder.add("AS setup")
        query_builder.add("ON exterior_temperature.ID_Station = setup.ID_Station")
        query_builder.execute()

    @staticmethod
    def create_synop():
        ext_temp_builder = QueryBuilder()
        ext_temp_builder.add_select("numer_sta, date AS timestamp, t")
        ext_temp_builder.add_from_name(EasyTableName.get_synop())
        ext_temp_builder.add_where("date >= '2016-05-01 00:00:00'")

        gateway_zip_builder = QueryBuilder()
        gateway_zip_builder.add_select("gateway_id, zip")
        gateway_zip_builder.add_from_name(EasyTableName.get_consumption("setup"))

        zip_station_builder = QueryBuilder()
        zip_station_builder.add_select_all()
        zip_station_builder.add_from_name(EasyTableName.get_utils_table_name("zip_synop_station"))

        query_builder = QueryBuilder()
        query_builder.add_create_replace(EasyTableName.get_consumption("exterior_temperature"))
        query_builder.add_select("gateway_id, timestamp, t AS value")
        query_builder.add_from(ext_temp_builder.get())
        query_builder.add("AS exterior_temperature")
        query_builder.add_full_outer_join_table(zip_station_builder.get())
        query_builder.add("AS station_zip ON exterior_temperature.numer_sta = station_zip.ID")
        query_builder.add_full_outer_join_table(gateway_zip_builder.get())
        query_builder.add("AS setup ON setup.zip = station_zip.zip_code")
        query_builder.execute()

    @staticmethod
    def select_rawdevicestate(query_builder):
        query_builder.add_select_all()
        query_builder.add_from_name(DbManager.rawdevicestate_consumption)
        query_builder.add_where()
        query_builder.add_filter_before_date_from_timestamp(cfg['dataset']['secure_pulling_days'])
        if variable_cfg['prune_year']:
            query_builder.add_and()
            query_builder.add_filter_between_date_from_timestamp_from_partition(variable_cfg['partition_year'])
        return query_builder

    @staticmethod
    def select_historical(query_builder, name):
        config = EasyTableName.historical_table_config[name]
        query_builder.add_select_historical_field_cast(config["type"])
        query_builder.add_from_name(DbManager.rawdevicestate_consumption)
        query_builder.add_where()
        query_builder.add_filter_parameter_oid(config["oid"])
        query_builder.add_and()
        query_builder.add_filter_before_date_from_timestamp(cfg['dataset']['secure_pulling_days'])
        if variable_cfg['prune_year']:
            query_builder.add_and()
            query_builder.add_filter_between_date_from_timestamp_from_partition(variable_cfg['partition_year'])
        return query_builder

    @staticmethod
    def select_openweathermap(query_builder, name):
        config = EasyTableName.historical_table_config[name]
        query_builder.add_select_weather_field_cast(name, config["type"])
        query_builder.add_from_name(DbManager.openweathermap)
        query_builder.add_where()
        query_builder.add_filter_before_date_from_timestamp(cfg['dataset']['secure_pulling_days'])
        if variable_cfg['prune_year']:
            query_builder.add_and()
            query_builder.add_filter_between_date_from_timestamp_from_partition(variable_cfg['partition_year'])
        return query_builder

    @staticmethod
    def select_static(query_builder, name):
        config = EasyTableName.static_table_name_list[name]
        query_builder.add_select_all()
        query_builder.add_from_name(EasyTableName.get_bucket(config['name']))
        return query_builder

