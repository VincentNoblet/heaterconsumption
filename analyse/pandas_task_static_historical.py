from config import d6tflow
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from analyse.pandas_task_historical import TaskGetHistorical
from analyse.pandas_task_static import TaskFeatureFactory, TaskConvertFilterLabel
from manager.query_customize import QueryBuilder, EasyQuery
from sklearn.model_selection import KFold, StratifiedKFold
from sklearn.base import clone as model_clone
from manager.bigquery_structure import EasyTableName
import joblib

def L1(model, X, y_true):
    pred = model.predict(X)
    diff = (np.abs(pred - y_true))
    return diff


def R2_score(Y_pred, Y_true):
    Y_mean = Y_true.mean()
    return 1 - ((Y_pred - Y_true) ** 2).sum() / ((Y_true - Y_mean) ** 2).sum()


def L1_relative(model, X, y_true):
    pred = model.predict(X)
    diff = np.abs((pred - y_true) / y_true)
    return diff


def L1_relative_pred(model, X, y_true):
    pred = model.predict(X)
    diff = (np.abs(pred - y_true) / pred)
    return diff


class TaskGetForeignHeating(d6tflow.tasks.TaskPqPandas):
    def requires(self): return TaskConvertFilterLabel("setup")

    def run(self):
        setup = self.input().load()
        setup.index = setup.setup_gateway_id

        foreign_heating_julien = EasyQuery.get_table(EasyTableName.get_utils_table_name("chimney_suspicion"))
        foreign_heating_julien["indicateur_julien"] = [1 for _ in foreign_heating_julien.values]
        foreign_heating_julien.index = foreign_heating_julien.gateway_id
        foreign_heating_julien = foreign_heating_julien.drop("gateway_id", axis=1)

        foreign_heating_hugo_builder = QueryBuilder()
        foreign_heating_hugo_builder.add_select_all()
        foreign_heating_hugo_builder.add_from_name(EasyTableName.get_utils_table_name("chimney_suspicion_hugo"))
        foreign_heating_hugo = foreign_heating_hugo_builder.get_execute().to_dataframe()
        foreign_heating_hugo.index = foreign_heating_hugo.gateway_id
        foreign_heating_hugo = foreign_heating_hugo.drop("gateway_id", axis=1)

        cheminee = pd.merge(pd.DataFrame([], index=setup.index), foreign_heating_julien, left_index=True,right_index=True, how="left")
        cheminee = pd.merge(cheminee, foreign_heating_hugo, left_index=True, right_index=True, how="left")
        cheminee = cheminee.fillna(0)
        self.save(cheminee)


class TaskGetCompletion(d6tflow.tasks.TaskPqPandas):
    def requires(self): return TaskConvertFilterLabel("setup")

    def run(self):
        setup = self.input().load()
        setup.index = setup.setup_gateway_id
        completion = EasyQuery.get_table(EasyTableName.get_utils_table_name("complete_installation"))
        completion.index = completion.id.transform(lambda x: x[6:])
        completion = completion.drop("id", axis=1)
        completion = pd.merge(pd.DataFrame([], index=setup.index), completion, left_index=True, right_index=True, how="left")
        completion = completion.fillna(0)
        self.save(completion)


class TaskGetProductFeature(d6tflow.tasks.TaskPqPandas):
    def run(self):
        data = EasyQuery.get_table(EasyTableName.get_utils_table_name("product_characterization"))
        self.save(data)


class TaskGetIsolation(d6tflow.tasks.TaskPqPandas):
    def run(self):
        query_builder = QueryBuilder()
        query_builder.add_select("gateway AS gateway_id, delta_t_conso_null AS b, coef AS a, R2 AS r2, nb_week AS nb_week")
        query_builder.add_from_name(EasyTableName.get_utils_table_name("isolation"))
        isolation = query_builder.get_execute().to_dataframe()
        isolation.index = isolation['gateway_id']
        isolation = isolation.drop("gateway_id", axis=1)
        self.save(isolation)


class TaskGetDeltaT0(d6tflow.tasks.TaskPqPandas):
    def run(self):
        isolation = EasyQuery.get_table(EasyTableName.get_utils_table_name("delta_t0"))
        isolation.index = isolation['gateway_id']
        isolation = isolation.drop("gateway_id", axis=1)
        self.save(isolation)


class TaskGetUsage(d6tflow.tasks.TaskPqPandas):
    def run(self):
        usage = EasyQuery.get_table(EasyTableName.get_utils_table_name("usage"))
        usage.index = usage['gateway_id']
        isolation = usage.drop("gateway_id", axis=1)
        self.save(isolation)


class TaskMergeStaticHistorical(d6tflow.tasks.TaskPqPandas):
    def requires(self): return {"feature": TaskFeatureFactory(),
                                "setup": TaskConvertFilterLabel("setup"),
                                'isolation': TaskGetIsolation(),
                                'delta_t0': TaskGetDeltaT0(),
                                'historical': TaskGetHistorical(),
                                'carac_model': TaskGetProductFeature(),
                                'foreign_heating': TaskGetForeignHeating(),
                                "completion": TaskGetCompletion()
                                }

    def run(self):
        feature = self.input()["feature"].load().drop("maj_format", axis=1)
        isolation = self.input()["isolation"].load()
        delta_t0 = self.input()["delta_t0"].load()
        carac_model = self.input()["carac_model"].load()
        historical = self.input()["historical"].load()
        foreign_heating = self.input()["foreign_heating"].load()
        completion = self.input()["completion"].load()
        setup = self.input()["setup"].load()
        setup = setup[["setup_gateway_id", "setup_Superficie", "setup_tenement_type_english", "setup_tenement_build_date_english", "setup_zone_climatique",  "setup_zone_climatique_details"]]
        setup.index = setup.setup_gateway_id
        setup.drop("setup_gateway_id", axis=1)
        model_list = carac_model["model"]
        carac_model = carac_model.drop("model", axis=1)
        carac_model = carac_model.T
        carac_model.columns = model_list

        data = pd.DataFrame([], index=setup.index)
        data = pd.merge(data, historical, left_index=True, right_index=True, how="inner")
        data = pd.merge(data, feature, left_index=True, right_index=True, how="inner")
        data = pd.merge(data, delta_t0, left_index=True, right_index=True, how="inner")
        data = pd.merge(data, isolation, left_index=True, right_index=True, how="inner")
        data = pd.merge(data, foreign_heating, left_index=True, right_index=True, how="inner")
        data = pd.merge(data, completion, left_index=True, right_index=True, how="inner")
        data = pd.merge(data, setup, left_index=True, right_index=True, how="inner")
        print("Data size after merge: " + str(data.shape[0]) + " values")

        count_list = ["count_device", "count_bedroom", "count_room", "count_connected", "count_wire_pilot"]
        data[count_list] = data[count_list].fillna(0)
        mode_list = ["standby", "auto", "internal", "basic", "external"]
        data[mode_list] = data[mode_list].fillna(0)
        for mode in mode_list:
            data[mode] = data[mode] / data["dataflow"]

        data[count_list] = data[count_list].fillna(0)
        data[list(carac_model.index)] = data["maj_model"].replace(np.nan, None).transform(lambda x: carac_model[x])
        self.save(data)


class ScoreAnalyzer:
    data_path = "data/data.pkl"
    model_path = "data/model.pkl"

    def __init__(self):
        self.model_score = []
        self.feature_importances = []

    def invalidate_merge_static_historical(self):
        TaskMergeStaticHistorical().invalidate()

    def update_data(self, feature_list, drop_incomplete=False, divide_superficie=True):
        d6tflow.preview(TaskMergeStaticHistorical())
        d6tflow.run(TaskMergeStaticHistorical())
        data = TaskMergeStaticHistorical().output().load()

        if divide_superficie: data["consumption"] = data["consumption"] / data["setup_Superficie"]

        filter_list = []
        filter_list = self.add_evaluate_filter(data, filter_list, self.custom_filter, **dict(filter_complete=drop_incomplete))
        filter_list = self.add_evaluate_filter(data, filter_list, self.consumption_filter, **dict(min_wh_m2=7000 * 20 ** (1 - divide_superficie), max_wh_m2=350000 * 300 ** (1 - divide_superficie)))
        filter_list = self.add_evaluate_filter(data, filter_list, self.dataflow_filter, **dict(dataflow_rate=0.8))
        filter_list = self.add_evaluate_filter(data, filter_list, self.superficie_filter, **dict(min_m2=20, max_m2=350))
        filter_list = self.add_evaluate_filter(data, filter_list, self.empty_setup_filter)
        filter_list = self.add_evaluate_filter(data, filter_list, self.maj_model_filter)

        filter = np.logical_and.reduce(filter_list)

        data = data[filter]
        print(data.columns, feature_list)
        data = data[data.columns.intersection(feature_list)]
        data = data.dropna()
        print("Data size after dropna: " + str(data.shape[0]) + " values")

        data = self.categoric_to_numeric(data)
        self.save_data(data)

    @staticmethod
    def add_evaluate_filter(data, filter_list, filter_f, **kwargs):
        _filter = filter_f(data, **kwargs)
        filter_size_before = np.logical_and.reduce(filter_list)
        filter_list.append(_filter)
        filter_size_after = np.logical_and.reduce(filter_list)
        print('Filter data loss ABSOLUTE: ' + str(np.logical_not(_filter).sum()) + "\n")
        print('Filter data loss BEFORE ' + str(filter_size_before.sum()) + ", AFTER:  " + str(filter_size_after.sum()) + "\n")
        return filter_list

    @staticmethod
    def empty_setup_filter(data):
        print("Drop count_device < 1 and count_wire > 0")
        return np.logical_and(data["count_connected"] >= 1, data["count_wire_pilot"] <= 0)

    @staticmethod
    def custom_filter(data, filter_complete):
        filter_list = [data.all_year >= 2017]
        # filter_list = [data.all_year == 2019]
        if filter_complete: filter_list.append(data.EchantillonComplet.apply(lambda x: bool(x)))
        # filter_list.append(data.nb_week > 10)
        # filter_list.append(np.logical_and(data.a > 5000, data.a < 100000))
        # filter_list.append(np.logical_and(data.b > 0, data.b < 10))
        # filter_list.append(data.indicateur_julien.apply(lambda x: not (bool(x))))
        # filter_list.append(data.r2 > 0.5)
        print("Custom filter: ")
        return np.logical_and.reduce(filter_list)

    @staticmethod
    def maj_model_filter(data):
        print("maj_model filter")
        return np.logical_and.reduce([data["maj_model"] != "Accessio", data["maj_model"] != "Baleares", data["maj_model"] != "Ipala", data["maj_model"].notna()])

    @staticmethod
    def superficie_filter(data, min_m2, max_m2):
        print("Drop superficie NA")
        return np.logical_and(data.setup_Superficie >= min_m2, data.setup_Superficie <= max_m2)

    @staticmethod
    def consumption_filter(data, min_wh_m2, max_wh_m2):
        print('dropping consumption rate under ' + str(round(min_wh_m2, 4)) + "Wh and above " + str(round(max_wh_m2, 1)) + "Wh")
        return np.logical_and(data["consumption"] > min_wh_m2, data["consumption"] < max_wh_m2)

    @staticmethod
    def dataflow_filter(data, dataflow_rate):
        print('dropping dataflow rate under ' + str(int(100 * dataflow_rate)) + "%")
        return data.dataflow >= dataflow_rate

    def categoric_to_numeric(self, data):
        self.replace_dict = {
            'setup_tenement_type_english': {'House': 1, 'Appartment': 0},
            'setup_tenement_build_date_english': {'1800-1948': 0, '1948-1974': 1, '1975-1988': 2, '1989-2005': 3, '2006-2012': 4, 'NewBuild': 5},
            'Réactivité': {'Faible': 0, 'Moyen': 1, 'Fort': 2},
            'Simple/Double': {'Double': 1, 'Simple': 0}
        }
        data = data.replace(self.replace_dict)
        non_numeric_cat_cols = ["setup_zone_climatique"]
        data = pd.concat([data, pd.get_dummies(data[non_numeric_cat_cols])], axis=1)
        data = data.drop(non_numeric_cat_cols, axis=1)
        return data

    def init_fit_folder(self, model_instance_list):
        data = self.load_data()
        X, Y = data.drop("consumption", axis=1), data["consumption"]
        n_model, p_splits = len(model_instance_list), 5
        model_array = np.ndarray(shape=(n_model, p_splits), dtype=object)
        for i, _model in enumerate(model_instance_list):
            for j in range(p_splits):
                model_array[i, j] = model_clone(_model)
        n_quantile = 10
        classified_Y = pd.cut(Y, bins=Y.quantile(np.linspace(0, 1, n_quantile + 1)).values, labels=[i for i in range(n_quantile)], include_lowest=True)
        folder = StratifiedKFold(n_splits=p_splits, shuffle=True).split(X, classified_Y)
        #folder = KFold(n_splits=p_splits, shuffle=True).split(X, Y)
        return {'folder': list(folder), "model_array": model_array, "X": X, "Y": Y}

    def fit_folder_evaluate(self, init_object):
        folder, X, Y = init_object["folder"], init_object["X"], init_object["Y"]
        model_array = init_object["model_array"]
        n_model, p_split = len(model_array), len(model_array[0])
        train_score, test_score = np.zeros([n_model, p_split]), np.zeros([n_model, p_split])
        for j, (train_index, test_index) in enumerate(folder):
            X_test, Y_test, X_train, Y_train = X.iloc[test_index], Y.iloc[test_index], X.iloc[train_index], Y.iloc[train_index]
            for i in range(n_model):
                model = model_array[i, j]
                model.fit(X_train, Y_train)
                train_score[i, j] = R2_score(model.predict(X_train), Y_train)
                test_score[i, j] = R2_score(model.predict(X_test), Y_test)
                try:
                    self.feature_importances.append(pd.Series(model.feature_importances_, index=X.columns).sort_values())
                except:
                    try:
                        self.feature_importances.append(pd.Series(model.coef_, index=X.columns).sort_values())
                    except:
                        None
                self.save_model(model)
        return {"train_score": train_score.mean(axis=1), "test_score": test_score.mean(axis=1), "train_std": train_score.std(axis=1), "test_std": test_score.std(axis=1)}

    def plot_hyper_parameter_score(self, model_instance, variable_name, variable_value, n_loop=2):
        score_loop = np.zeros([len(variable_value), n_loop])
        init_object = self.init_fit_folder([model_instance])
        for j in range(n_loop):
            for (i, value) in enumerate(variable_value):
                for model in init_object["model_array"][0]:
                    model.set_params(**{variable_name: value})
                score_loop[j, i] = self.fit_folder_evaluate(init_object)["test_score"]
        plt.plot(variable_value, score_loop.mean(axis=1))

    def add_model_score(self, model_instance_list):
        init_object = self.init_fit_folder(model_instance_list)
        score = self.fit_folder_evaluate(init_object)
        self.model_score.append(score["test_score"][0])

    def plot_model_score(self, model_instance_list):
        init_object = self.init_fit_folder(model_instance_list)
        score = self.fit_folder_evaluate(init_object)
        ind = np.arange(len(model_instance_list))
        train_bar = plt.bar(ind, score["train_score"], 0.35, bottom=0, yerr=score["train_std"] / 2)
        test_bar = plt.bar(ind, score["test_score"], 0.35, bottom=0, yerr=score["test_std"] / 2)
        plt.xticks(ind, list(map(lambda model: type(model).__name__, model_instance_list)))
        plt.yticks(np.arange(0, 1.1, 0.1))
        plt.ylabel('Score $R^{2}$')
        plt.title('Score by model')

        plt.legend((train_bar[0], test_bar[0]), ('train', 'test'))
        train_dict = dict(zip([type(instance).__name__ for instance in model_instance_list], [round(x, 3) for x in score["train_score"]]))
        test_dict = dict(zip([type(instance).__name__ for instance in model_instance_list], [round(x, 3) for x in score["test_score"]]))

        print("Train score: " + str(train_dict) + '\n' + "Test_score: " + str(test_dict))

    def fit_folder_evaluate_quantile(self, init_object):
        folder, X, Y = init_object["folder"], init_object["X"], init_object["Y"]
        model_array = init_object["model_array"]
        n_model, p_split = len(model_array), len(model_array[0])
        train_score, test_score = np.zeros(p_split), np.zeros(p_split)
        for j, (train_index, test_index) in enumerate(folder):
            X_test, Y_test, X_train, Y_train = X.iloc[test_index], Y.iloc[test_index], X.iloc[train_index], Y.iloc[train_index]
            for i in range(n_model):
                model = model_array[i, j]
                model.fit(X_train, Y_train)
            train_score[j] = np.logical_and(model_array[0, j].predict(X_train) <= Y_train, model_array[1, j].predict(X_train) >= Y_train).mean()
            test_score[j] = np.logical_and(model_array[0, j].predict(X_test) <= Y_test, model_array[1, j].predict(X_test) >= Y_test).mean()

        return {"model": model_array, "train_score": train_score.mean(), "test_score": test_score.mean(), "train_std": train_score.std(), "test_std": test_score.std()}

    def plot_model_score_quantile(self, model_instance_list):
        init_object = self.init_fit_folder(model_instance_list)
        score = self.fit_folder_evaluate_quantile(init_object)
        print("Train score: " + str(score["train_score"]) + '\n' + "Test_score: " + str(score["test_score"]))
        return score["model"]

    def get_compare_score_on_drop(self, model_instance_list, variable):
        init_object = self.init_fit_folder(model_instance_list)
        score_with = self.fit_folder_evaluate(init_object)
        init_object["X"] = init_object["X"].drop(variable, axis=1)
        score_without = self.fit_folder_evaluate(init_object)
        print(score_with)
        print(score_without)

    def save_data(self, data):
        joblib.dump(data, self.data_path)

    def load_data(self):
        return joblib.load(self.data_path)

    def save_model(self, model):
        joblib.dump(model, self.model_path)

    def load_model(self):
        return joblib.load(self.model_path)

    def see_important_feature(self):
        return self.feature_importances
